
function theme_home(){
	// primary-menu 
	jQuery('a.page-scroll, #block-aesthetic-main-menu li:first-child a').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = jQuery(this.hash);
          target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              jQuery('html,body').animate({
                  scrollTop: target.offset().top
              }, 1000);
              return false;
          }
      	}
  	});
	$("#block-aesthetic-main-menu ul li:first-child a").addClass("page-scroll");
    $("#block-aesthetic-main-menu ul li:first-child a").attr("href", "#home");
    $("#block-aesthetic-main-menu ul li:first-child a").focus();
}
jQuery(document).ready(function($){
	theme_home();	
});

